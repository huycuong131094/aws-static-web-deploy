import boto3
import datetime
from os import environ, listdir, path, walk
from botocore.exceptions import ClientError
import sys
import utils

def main():
    argLen = len(sys.argv)
    if argLen != 2:
        raise ValueError(
            'Require exacly 1 argument. Target directory need to upload to S3')

    targetDir: str = sys.argv[1]

    if not targetDir:
        raise ValueError('Require the target directory to upload to S3')

    emptyBucket()

    strippedTargetDir = targetDir.strip()
    syncResult = syncS3Bucket(strippedTargetDir)
    if not syncResult:
        raise RuntimeError(
            f'Error while trying to sync {strippedTargetDir} directory to s3')
    invalidateCloudFront()


def emptyBucket():
    bucketName = getBucketName()
    print(f'Start to empty bucket {bucketName}')
    s3 = boto3.resource('s3')
    bucket = s3.Bucket(bucketName)
    bucket.objects.all().delete()
    print(f'Complete empty bucket {bucketName}')


def invalidateCloudFront():
    distributionId = environ.get('AWS_DISTRIBUTION_ID', None)
    if not distributionId:
        raise RuntimeError(
            'Missing cloudfront distribution id for variable AWS_DISTRIBUTION_ID')

    print(f'Start to invalidate cloudfront distribution {distributionId}')
    cf = boto3.client('cloudfront')
    response = cf.create_invalidation(
        DistributionId=distributionId,
        InvalidationBatch={
            'Paths': {
                'Quantity': 1,
                'Items': [
                    '/*'
                ],
            },
            'CallerReference': str(datetime.datetime.now())
        }
    )
    print(
        f'Complete invalidate cloudfront distribution {distributionId}. \nResponse: {response}')


def syncS3Bucket(targetDir):
    bucketName = getBucketName()
    region = getBucketRegion()
    return syncToS3(targetDir, region, bucketName)


def getBucketName():
    bucketName = environ.get('AWS_BUCKET', None)
    if not bucketName:
        raise RuntimeError('Missing bucket name for variable AWS_BUCKET')
    return bucketName


def getBucketRegion():
    bucketRegion = environ.get('AWS_DEFAULT_REGION', None)
    if not bucketRegion:
        raise RuntimeError(
            'Missing bucket region for variable AWS_DEFAULT_REGION')
    return bucketRegion


def syncToS3(targetDir, region, bucketName):
    '''
        Assume that bucket already exist
    '''
    if not path.isdir(targetDir):
        raise ValueError(f'Target Dir {targetDir} not found.')

    allFiles = []

    for root, dirs, files in walk(targetDir):
        allFiles += [path.join(root, f) for f in files]

    totalFile = len(allFiles)

    if not totalFile:
        print(f'{targetDir} is a empty directory')
        return False

    print(f'Start to sync {targetDir} directory to bucket: {bucketName}')
    s3 = boto3.resource('s3', region_name=region)

    filesStr = '\n'.join(allFiles)
    print(f'All file in {targetDir} directory: \n{filesStr}')

    for filename in allFiles:
        mimetype = utils.guessFileType(filename)
        s3.Object(bucketName, path.relpath(filename, targetDir)).put(
            Body=open(filename, 'rb'),
            ContentType=mimetype
        )

    print(f'Complete sync {targetDir} directory to bucket: {bucketName}')

    return True


if __name__ == '__main__':
    main()
