FROM python:3-slim

RUN apt update -y && \
    apt install libmagic1 -y && \
    pip install python-magic && \
    pip install boto3

WORKDIR /source

COPY . .

ENTRYPOINT [ "python3", "deploy.py" ]