# What is the purpose of this image


# How to use this image
Use `/dist` directory as a place for the file need to upload to s3

Need to populate some environment when run this image:
- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- AWS_DEFAULT_REGION
- AWS_BUCKET
- AWS_DISTRIBUTION_ID


# How to install requirement
Build docker for default
Only use requirements.txt in windows