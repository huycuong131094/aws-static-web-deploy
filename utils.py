import mimetypes


def guessFileType(filename, buffer=None):
    mimetype, _ = mimetypes.guess_type(filename)
    if mimetype:
        return mimetype
    try:
        import magic
        if buffer:
            mimetype = magic.from_buffer(buffer, mime=True)
        else:
            mimetype = magic.from_file(filename, mime=True)
    except ImportError as e:
        print(f'Import Error {str(e)}')
        raise e
    return mimetype
